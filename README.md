# EIL Middleware
Runs as a service as the interface between the Panopto RRLightService and the Encore Indicator Light hardware.

## Version History
* v1.0.0.1 - 2020/07/06
  * Fix installer bugs
  * Reduce logging
* v1.0.0.0 - 2020/07/03
  * Initial release

## Building
You may build the binary and installer from this source code with the following tools. If you need to create a new version, update the version number both in AssemblyInfo.cs and Product.wxs.

* Visual Studio 2020
* WiX Toolset 3.11

## License
Copyright 2020 Panopto, Durham University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
