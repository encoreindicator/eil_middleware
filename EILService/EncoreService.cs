﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.IO.Pipes;
using System.IO;
using System.IO.Ports;
using System.Timers;
using System.Management;
using System.Threading;

using HidSharp;
using HidSharp.Experimental;
using HidSharp.Reports;
using HidSharp.Reports.Encodings;
using HidSharp.Utility;
using System.Runtime.CompilerServices;

namespace EILService
{
	public partial class EncoreService : ServiceBase
	{
		#region Structure Definitions

		private struct Light
		{
			public LightOutput lightState;
			public SerialPort port;
			public bool hasResponded;
			public int numberFails;
		}

		/// <summary>
		/// Structure for Operations with EIL light unit
		/// </summary>
		private struct LightOutput
		{
			public int redPrimary;
			public int redSecondary;
			public int greenPrimary;
			public int greenSecondary;
			public char animation;
			public int period;
			public int miscData; /// Chase (R) Width (0-8) or Flash (F) time delay (ms)
			public byte[] hidOutputLED;
		}

		private struct HIDLight
        {
			public HidDevice device;
			public Thread lightThread;
			public Queue<byte[]> lightWriteQueue;
			public int numberOfPresses;
			public bool isLongPress;
			public bool isHeld;
        }

		#endregion

		#region Class Variables

		bool useVerboseTrace;
        /// <summary>
        /// List of all connected EIL light units
        /// </summary>
        private List<SerialPort> portObjList = new List<SerialPort>();
		private List<Light> lightList = new List<Light>();
		private string masterCOM = "";

		List<HidDevice> hidLightList;
		List<HIDLight> hidLightStructList;
		List<Queue<byte[]>> writeQueueList;

		State currentState;
		LightOutput currentLightState;

        //private bool isConnected;
        //private bool isHeld;
        //private int numberOfPresses;
        //private bool isLongPress;

        //private System.Timers.Timer heldTimer;
        //private System.Timers.Timer releaseTimer;
        private System.Timers.Timer statePingTimer;
        private System.Timers.Timer flashEndTimer;
        private System.Timers.Timer chaseEndTimer;
        private System.Timers.Timer danMashTimer;

		//private Queue<byte[]> outputQueue;
		//private Queue<byte[]> inputQueue;

		/// <summary>
		/// FORMAT FOR LIGHT CODES: 
		/// Report ID | Animation Code | Primary: Intensity | R | G | B | Secondary: Intensity | R | G | B | Period | Duty Cycle | Chase Step/Direction
		/// Codes:
		/// 
		/// SOLID COLOURS:
		///  Dim Green Solid - new byte[] {0x01,0x01,0x7f,0x00,0xff,0x00,0x00,0x00,0x00,0x00}               NNS
		byte[] greenSolidDim = new byte[] { 0x01, 0x01, Properties.Settings.Default.PreviewingBrightness, 0x00, Properties.Settings.Default.GreenBrightness, 0x00 };
		///  Bright Green Solid - new byte[] {0x01,0x01,0xff,0x00,0xff,0x00,0x00,0x00,0x00,0x00}            WNS >5
		byte[] greenSolidBright = new byte[] { 0x01, 0x01, 0xff, 0x00, Properties.Settings.Default.GreenBrightness, 0x00 };
		///  Bright Red Solid - new byte[] {0x01,0x01,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00}              REC >5
		byte[] redSolidBright = new byte[] { 0x01, 0x01, 0xff, Properties.Settings.Default.RedBrightness, 0x00, 0x00 };
		///  Bright Orange Solid - new byte[] {0x01,0x01,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00}           PAU >5
		byte[] orangeSolidBright = new byte[] { 0x01, 0x01, 0xff, Properties.Settings.Default.AmberRedBrightness, Properties.Settings.Default.AmberGreenBrightness, 0x00 };
		///  
		/// BREATHES
		///  Green Breathe - new byte[] {0x01,0x03,0xff,0x00,0xff,0x00,0x00,0x00,0x00,0x00,0x14}            WNS <5
		byte[] greenBreathe = new byte[] { 0x01, 0x05, 0xff, 0x00, Properties.Settings.Default.GreenBrightness, 0x00, 0xff, 0x00, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.BreathePeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.BreathePeriod / 50)[1], 0xff, 0x06 };
		///  Red Breathe - new byte[] {0x01,0x03,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x14}              REC <5
		byte[] redBreathe = new byte[] { 0x01, 0x05, 0xff, Properties.Settings.Default.RedBrightness, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.BreathePeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.BreathePeriod / 50)[1], 0xff, 0x06 };
		///  Orange Breathe - new byte[] {0x01,0x03,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x14}           PAU <5
		byte[] orangeBreathe = new byte[] { 0x01, 0x05, 0xff, Properties.Settings.Default.AmberRedBrightness, Properties.Settings.Default.AmberGreenBrightness, 0x00, 0xff, 0x00, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.BreathePeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.BreathePeriod / 50)[1], 0xff, 0x06 };
		///  
		///  Green/Red Flash - new byte[] {0x01,0x01, 0xff,0x00,0xff,0x00, 0xff,0xff,0x00,0x00, 0x04, 0x7f}
		byte[] greenRedFlash = new byte[] { 0x01, 0x02, 0xff, 0x00, Properties.Settings.Default.GreenBrightness, 0x00, 0xff, Properties.Settings.Default.RedBrightness, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[1], 0x7f, 0x00 };
		///  Red/Orange Flash - new byte[] {0x01,0x01, 0xff,0xff,0xff,0x00, 0xff,0xff,0x00,0x00, 0x04, 0x7f}
		byte[] redOrangeFlash = new byte[] { 0x01, 0x02, 0xff, Properties.Settings.Default.AmberRedBrightness, Properties.Settings.Default.AmberGreenBrightness, 0x00, 0xff, Properties.Settings.Default.RedBrightness, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[1], 0x7f, 0x00 };
		///  Orange/Green Flash - new byte[] {0x01,0x01, 0xff,0x00,0xff,0x00, 0xff,0xff,0xff,0x00, 0x04, 0x7f}
		byte[] orangeGreenFlash = new byte[] { 0x01, 0x02, 0xff, 0x00, Properties.Settings.Default.GreenBrightness, 0x00, 0xff, Properties.Settings.Default.AmberRedBrightness, Properties.Settings.Default.AmberGreenBrightness, 0x00, BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[1], 0x7f, 0x00 };
		///  
		///  Green Chase - new byte[] {0x01,0x01, 0xff,0x00,0xff,0x00, 0x00,0x00,0x00,0x00, 0x0b, 0x7f, 0x08}
		byte[] greenChase = new byte[] { 0x01, 0x04, 0xff, 0x00, Properties.Settings.Default.GreenBrightness, 0x00, 0xff, 0x00, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.ChasePeriod / 400)[0], BitConverter.GetBytes(Properties.Settings.Default.ChasePeriod / 400)[1], 0b00110011, 0x06 };
		///  Red Chase - new byte[] {0x01,0x01, 0xff,0xff,0x00,0x00, 0x00,0x00,0x00,0x00, 0x0b, 0x7f, 0x08}
		byte[] redChase = new byte[] { 0x01, 0x04, 0xff, Properties.Settings.Default.RedBrightness, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.ChasePeriod / 400)[0], BitConverter.GetBytes(Properties.Settings.Default.ChasePeriod / 400)[1], 0b00110011, 0x06 };
		///  Orange Chase - new byte[] {0x01,0x01, 0xff,0xff,0xff,0x00, 0x00,0x00,0x00,0x00, 0x0b, 0x7f, 0x08}
		byte[] orangeChase = new byte[] { 0x01, 0x04, 0xff, Properties.Settings.Default.AmberRedBrightness, Properties.Settings.Default.AmberGreenBrightness, 0x00, 0xff, 0x00, 0x00, 0x00, BitConverter.GetBytes(Properties.Settings.Default.ChasePeriod / 400)[0], BitConverter.GetBytes(Properties.Settings.Default.ChasePeriod / 400)[1], 0b00110011, 0x06 };
		///  
		///  Dan Mash - 
		byte[] danMash = new byte[] { 0x01, 0x05, 0xff, 0xff, 0x00, 0x00, 0xff, 0x00, 0x00, 0xff, 0x10, 0x00, 0xff, 0x06 };
		///  Light Off - 
		byte[] lightOff = new byte[] { 0x01, 0x01, 0xff, 0x00, 0x00, 0x00 };
		///  Error State - 
		byte[] lightError = new byte[] { 0x01, 0x02, 0xff, 0x00, 0x00, 0x00, 0xff, Properties.Settings.Default.AmberRedBrightness, Properties.Settings.Default.AmberGreenBrightness, 0x00, BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[0], BitConverter.GetBytes(Properties.Settings.Default.FlashPeriod / 50)[1], 0x7f, 0x00 };
		///  Light Init
		byte[] lightInit = new byte[] { 0x01, 0x01, 0xff, 0x00, 0xff, 0x80 };
		///
		///  
		///  Sound Double Beep
		byte[] soundDoubleBeep = new byte[] { 0x02, 0x7f, 0, 0x8F, 0x05, 0x00, 0x00, 0x00, 60, 60, 60 };
		///  
		///  Motor Double Buzz - new byte[] { 0x03, 0x08, 0x10, 1, 0xfa, 0x00, 0x7f }
		byte[] motorDoubleBuzz = new byte[] { 0x03, 0x08, 0x10, 1, 0xfa, 0x00, 0x7f };
		///  
		///  
		///  
		/// </summary>

		private bool isRecording;
		private bool isPaused;
		private bool isTransitioning;
		private bool isExtend;
		private bool isDanMash;
		//private bool isErrored;
		private int numActiveLights;
		private int transitionType; /// 1 = Stopped to Rec, 2 = Rec to Stopped, 3 = Rec to Paused, 4 = Paused to Rec, 5 = Paused to Stopped, 6 = Stopped to Paused

		private int minsToNextRecording;

		/// Communicator
		NamedPipeServerStream pipeServerOut;
		NamedPipeServerStream pipeServerIn;
		StreamReader inputReader;
		StreamWriter outputWriter;

		Queue<string> writerQueue;

		//NamedPipeClientStream pipeClient;

		Thread readThreadP;
		Thread writeThreadP;
		private ManualResetEvent inputQueueEvent = new ManualResetEvent(initialState: false);

		bool isConnectedToRecorder;

		DateTime lastPipeReadTime;

		#endregion

		#region Constructor and Initialisation

		public EncoreService()
		{
			InitializeComponent();
		}

		public void RunAsProgram()
		{
			this.OnStart(null);
		}

		protected override void OnStart(string[] args)
		{
			useVerboseTrace = true;
			VerboseTrace("[MAIN] ------------------------------------");
			Trace.TraceInformation("[MAIN] Encore Service Starting");

			currentState = State.Init;
			minsToNextRecording = 10000;
			currentLightState = new LightOutput();
			currentLightState.redPrimary = 0;
			currentLightState.redSecondary = 0;
			currentLightState.greenPrimary = 0;
			currentLightState.greenSecondary = 0;
			currentLightState.animation = 'O';
			currentLightState.period = 0;
			currentLightState.miscData = 0;
			currentLightState.hidOutputLED = lightOff;
			isRecording = false;
			isPaused = false;
			isTransitioning = false;
			isExtend = false;
			isDanMash = false;
			//isErrored = false;

            //isConnected = false;
            //isHeld = false;
            //isLongPress = false;
            //numberOfPresses = 0;

            //outputQueue = new Queue<byte[]>();

            /// Timers
            //heldTimer = new System.Timers.Timer(3000);
            //heldTimer.Elapsed += heldTimerElapsedEvent;

            //releaseTimer = new System.Timers.Timer(1000);
            //releaseTimer.Elapsed += releaseTimerElapsedEvent;

            statePingTimer = new System.Timers.Timer(30000); // Once a Minute
            statePingTimer.Elapsed += StatePingTimerEvent;
            statePingTimer.AutoReset = true;
            statePingTimer.Enabled = true;

            flashEndTimer = new System.Timers.Timer(Properties.Settings.Default.FlashNum * Properties.Settings.Default.FlashPeriod);
            flashEndTimer.Elapsed += FlashEndTimerEvent;

            chaseEndTimer = new System.Timers.Timer(Properties.Settings.Default.ChasePeriod * 3);
            chaseEndTimer.Elapsed += ChaseEndTimerEvent;

            danMashTimer = new System.Timers.Timer(5000);
            danMashTimer.Elapsed += DanMashTimerEvent;


			/// Connect HID Devices

			hidLightList = new List<HidDevice>();
			hidLightStructList = new List<HIDLight>();
			writeQueueList = new List<Queue<byte[]>>();

			Thread hidThread = new Thread(new ThreadStart(ConnectHid));
			hidThread.IsBackground = true;
			hidThread.Start();

			

			/// Connect Serial Devices

			portObjList = new List<SerialPort>();
			lightList = new List<Light>();
			

			ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort");
			var mao = managementObjectSearcher.Get();

			foreach (ManagementObject currentObject in managementObjectSearcher.Get())
			{
				if (currentObject["PNPDeviceID"] != null)
				{
					if (currentObject["PNPDeviceID"].ToString().Contains("VID_04D8&PID_EE61"))
					{
						try
						{
							SerialPort portObj = new SerialPort(
								currentObject["DeviceID"].ToString(),
								9600,
								(Parity)Enum.Parse(typeof(Parity), "None", true),
								8,
								(StopBits)Enum.Parse(typeof(StopBits), "One", true));

							portObj.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
							portObj.DtrEnable = true;
							portObj.RtsEnable = true;

							portObj.Open();
							portObjList.Add(portObj);
							Light temp_light = new Light();
							temp_light.port = portObj;
							lightList.Add(temp_light);
							if (portObjList.Count == 1)
							{
								portObj.WriteLine("Unit-SetMaster");
								masterCOM = portObj.PortName;
							}
							else
							{
								portObj.WriteLine("Unit-SetSlave");
							}

							portObj.WriteLine("Unit-SetRedBrightness: " + 4095);
							portObj.WriteLine("Unit-SetGreenBrightness: " + 4095);
							portObj.WriteLine("Unit-SetAmberRedBrightness: " + 4095);
							portObj.WriteLine("Unit-SetAmberGreenBrightness: " + 820);
							portObj.WriteLine("Unit-SetMOGBrightness: " + 10);
							portObj.WriteLine("Unit-SetGreenRecord: " + false);
							portObj.WriteLine("Unit-SetStopHeld: " + 3000);
							portObj.WriteLine("Unit-SetPauseThreshold: " + 1000);
							portObj.WriteLine("Unit-SetFlashPeriod: " + 1000);
							portObj.WriteLine("Unit-SetFlashNum: " + 3);
							portObj.WriteLine("Unit-SetBreathePeriod: " + 5000);
							portObj.WriteLine("Unit-SetBreatheMins: " + 5);
							portObj.WriteLine("Unit-SetChasePeriod: " + 1500);
						}
						catch (Exception)
						{
							//???
						}
					}
				}
			}

			/// Start Pipes

			VerboseTrace("[EIL-R] Starting Pipes");
			
			//outputWriter.AutoFlush = false;

			writerQueue = new Queue<string>();
			readThreadP = new Thread(NamedPipeReaderThread);
			writeThreadP = new Thread(NamedPipeWriterThread);
			//readThreadP.IsBackground = true ;
			//writeThreadP.IsBackground = true;
			readThreadP.Start();

			Trace.TraceInformation("[EIL-R] Started Encore Service Succesfully");
		}

        protected override void OnStop()
		{
			/// Destroy timers, threads and close serial connections

			Trace.TraceInformation("[MAIN] Encore Service Closing");
			
			statePingTimer.Stop();
			statePingTimer.Dispose();

			flashEndTimer.Stop();
			flashEndTimer.Dispose();

			chaseEndTimer.Stop();
			chaseEndTimer.Dispose();

			danMashTimer.Stop();
			danMashTimer.Dispose();

			this.Output("Unit-Stop");
			foreach (SerialPort portObj in portObjList)
			{
				portObj.Close();
			}
		}

        #endregion

        #region HID Device Handling

        private void ConnectHid()
		{
			var list = DeviceList.Local;

			list.Changed += HIDDeviceListChangedEvent;

			HIDDeviceConnector();
		}


        private void HIDDeviceListChangedEvent(Object source, DeviceListChangedEventArgs e)
        {
			VerboseTrace("[MAIN] Device list changed");
			HIDDeviceConnector();
		}

		private void HIDDeviceConnector()
        {
			var list = DeviceList.Local;
			var hidDeviceList = list.GetHidDevices().ToArray();
			VerboseTrace(string.Format("[MAIN] Found {0} HIDs", hidDeviceList.Length));

			List<HidDevice> hidLightListTemp = new List<HidDevice>();
			foreach (HidDevice dev in hidDeviceList)
			{
				if (dev.ProductID.Equals(0xed0e) && dev.VendorID.Equals(0x04d8))    // TODO Check
				{
					VerboseTrace("[MAIN] Found Device");
					hidLightListTemp.Add(dev);
				}
			}

			List<HidDevice> hidLightListTempRemove = new List<HidDevice>();

			for (int i = 0; i < hidLightList.Count; i++)
			{
				HidDevice dev = hidLightList[i];
				if (!hidLightListTemp.Contains(dev))
				{
					hidLightListTempRemove.Add(dev);
				}
			}
			
			foreach(HidDevice dev in hidLightListTempRemove)
            {
				// TODO Shutdown of light
				foreach(HIDLight light in hidLightStructList)
                {
					if(light.device.Equals(dev))
                    {
						writeQueueList.Remove(light.lightWriteQueue);
						light.lightThread.Abort();

						hidLightStructList.Remove(light);
						break;
                    }
                }
				hidLightList.Remove(dev);
            }

			foreach (HidDevice dev in hidLightListTemp)
            {
				if(!hidLightList.Contains(dev))
                {
					hidLightList.Add(dev);
					HIDLight newHidLight = new HIDLight();
					
					newHidLight.device = dev;
					
					Queue<byte[]> newWriterQueue = new Queue<byte[]>();
					newHidLight.lightWriteQueue = newWriterQueue;
					writeQueueList.Add(newWriterQueue);
					hidLightStructList.Add(newHidLight);

					Thread hidLightThread = new Thread(() => RunHidLight(newHidLight));
					hidLightThread.IsBackground = true;
					hidLightThread.Start();
					newHidLight.lightThread = hidLightThread;
				}
            }
		}

		private void RunHidLight(HIDLight lightStruct)
        {
			System.Timers.Timer heldTimer = new System.Timers.Timer(Properties.Settings.Default.TimeLongPress);
			heldTimer.Elapsed += (sender, e) => heldTimerElapsedEvent(sender, e, heldTimer, lightStruct.device);
			System.Timers.Timer releaseTimer = new System.Timers.Timer(Properties.Settings.Default.TimeDoublePressThreshold);
			releaseTimer.Elapsed += (sender, e) => releaseTimerElapsedEvent(sender, e, releaseTimer, lightStruct.device);
       
			try
			{
				// Opening Device

				HidStream hidStream;
				HidDevice device = lightStruct.device;
				Trace.TraceInformation("Connecting to EIL version " + device.ReleaseNumber.ToString());
				if (device.TryOpen(out hidStream))
				{
					
					hidStream.ReadTimeout = Timeout.Infinite;

					using (hidStream)
					{
						var inputReportBuffer = new byte[device.GetMaxInputReportLength()];
						var reportDescriptor = device.GetReportDescriptor();
						var inputReceiver = reportDescriptor.CreateHidDeviceInputReceiver();

						inputReceiver.Start(hidStream);

						VerboseTrace("[HID] Started HID device");

						bool breakPressed = false;

						hidStream.Write(lightInit);
						hidStream.Write(new byte[] { 0x02, 0x7f, 0, 0x8F, 0x01, 0x00, 0x00, 0x00, 60 });

						while (true)
						{
							

							if (inputReceiver.WaitHandle.WaitOne(10))
							{
								if (!inputReceiver.IsRunning) { break; } // Disconnected?

								Report report;
								while (inputReceiver.TryRead(inputReportBuffer, 0, out report))
								{
									VerboseTrace("[HID] Device Recieved Message" + BitConverter.ToString(inputReportBuffer));

									if (inputReportBuffer[0].Equals(0x01))
									{
										/// update light struct
										int lightStructNum = 0;
										for (lightStructNum = 0; lightStructNum < hidLightStructList.Count; lightStructNum++)
										{
											HIDLight lightStructTemp = hidLightStructList[lightStructNum];
											if (lightStructTemp.device.Equals(device))
											{
												lightStruct = lightStructTemp;
												break;
											}
										}

										if (inputReportBuffer[1].Equals(0x01))
										{
											lightStruct.isHeld = true;
											heldTimer.Start();
											releaseTimer.Stop();
											lightStruct.numberOfPresses += 1;
											VerboseTrace("[HID] Number of Presses: " + lightStruct.numberOfPresses);
										}
										else if (inputReportBuffer[1].Equals(0x00))
										{
											lightStruct.isHeld = false;
											heldTimer.Stop();
											releaseTimer.Start();
										}
										else
										{
											throw new Exception("Unexpected Input Error");
										}

										hidLightStructList[lightStructNum] = lightStruct;
									}
									else if (inputReportBuffer[0].Equals(0x02))
									{
										VerboseTrace("[HID] Warning- gestures are not supported", 1);
									}
									else if (inputReportBuffer[0].Equals(0xf0))
									{
										/// Interlock Error
										string errorType = "";
										byte errorCode = inputReportBuffer[1];
										if ((errorCode & 0b00000100) > 0)
										{
											errorType += " Motor Errored;";
										}
										if ((errorCode & 0b00000010) > 0)
										{
											errorType += " Buzzer Errored;";
										}
										if ((errorCode & 0b00000001) > 0)
										{
											errorType += " Light Errored";
										}
										Trace.TraceError("[HID] Interlock Error:" + errorType);
										// TODO Handle Properly
									}

									inputReportBuffer = new byte[device.GetMaxInputReportLength()]; // Clear buffer
								}
							}

							if (breakPressed) { break; } // Stay open for 20 seconds.

							// Send Messages Here
							if (lightStruct.lightWriteQueue.Count > 0)
							{
								var messageSent = lightStruct.lightWriteQueue.Dequeue();
								hidStream.Write(messageSent);
								VerboseTrace("[MAIN] HID output: " + BitConverter.ToString(messageSent));
							}
						}
					}
					Trace.TraceInformation("Closed device.");
				}
				else
				{
					Trace.TraceInformation("Failed to open device.");
				}
			}
			catch (Exception ex)
			{
				VerboseTrace(ex.GetType().ToString());
			}

            /// Tidy Up and Delete Itself

            heldTimer.Stop();
            heldTimer.Dispose();

            releaseTimer.Stop();
            releaseTimer.Dispose();

            hidLightList.Remove(lightStruct.device);
			writeQueueList.Remove(lightStruct.lightWriteQueue);
			hidLightStructList.Remove(lightStruct);
			//isConnected = false;
		}

		private void HidOutput(byte[] writeLine)
        {
			foreach(HIDLight light in hidLightStructList)
            {
				light.lightWriteQueue.Enqueue(writeLine);
            }
        }

		#endregion

		#region Light Service Communicator

		private void NamedPipeReaderThread()
		{
			isConnectedToRecorder = false;
			
			while (true)
			{
				Thread.Sleep(1000);
				pipeServerOut = new NamedPipeServerStream("ENCORE_SERVICE_MAIN_PIPE_OUT", PipeDirection.InOut);
				pipeServerIn = new NamedPipeServerStream("ENCORE_SERVICE_MAIN_PIPE_IN", PipeDirection.InOut);
				inputReader = new StreamReader(pipeServerIn);
				outputWriter = new StreamWriter(pipeServerOut);

				Trace.TraceInformation("[EIL-R] Waiting to Connect to Pipe...");
				pipeServerIn.WaitForConnection();
				Trace.TraceInformation("[EIL-R] Incoming Pipe Connected");
				pipeServerOut.WaitForConnection();
				Trace.TraceInformation("[EIL-R] Outgoing Pipe Connected");
				outputWriter.Flush();
				outputWriter.WriteLine("_init_");
				Thread.Sleep(500);
				outputWriter.Flush();
				if (!inputReader.ReadLine().Equals("_init_"))
				{
					Trace.TraceError("[EIL-R] Connection to Light Service failed due to incorrect pipe response");
					continue;
				}

				isConnectedToRecorder = true;
				lastPipeReadTime = DateTime.Now.ToLocalTime();
				if(!writeThreadP.IsAlive)
                {
					writeThreadP.Start();
				}
				
				VerboseTrace("[EIL-R] Checking for Reads");
				while (isConnectedToRecorder)
				{
					try
					{
						//Trace.TraceInformation("[EIL-R] Peek: " + inputReader.Peek());
						if (inputReader.Peek() >= -1)
						{
							string newInput = inputReader.ReadLine();
							if(newInput == null)
                            {
								Trace.TraceError("[EIL-R] Light Service has stopped running in a smooth fashion...");
								isConnectedToRecorder = false;
								break;
							}

							string[] newInputs = newInput.Split('|');
							VerboseTrace("[EIL-R] Read Made: " + newInput);
							lastPipeReadTime = DateTime.Now.ToLocalTime();

							

							bool mustUpdateValues = false;
							if (newInputs[0].Equals("_Close_"))
							{
								Trace.TraceError("[EIL-R] Light Service has stopped running in a smooth fashion...");
								isConnectedToRecorder = false;
							}
							else if (newInputs[0].Equals("_Service Failure_"))
							{
								Trace.TraceError("[EIL-R] Could not Opt Out as expected to be able to");
							}
							else if (newInputs[0].Equals("Result-CommandExtend"))
							{
								isExtend = true;
								currentLightState = UpdateLightValues();
								chaseEndTimer.Start();
							}
							else if (newInputs[0].Equals("Result-State"))
							{
								//newInput = inputReader.ReadLine();
								//Trace.TraceInformation("[EIL-R] Read Made: " + newInput);
								State newCurrentState;
								if (Enum.TryParse(newInputs[1], true, out newCurrentState))
								{
									if (!isTransitioning)
									{
										isTransitioning = HasTransitioned(newCurrentState);
									}
									if (isTransitioning)
									{
										minsToNextRecording = 30000;
										//flashEndTimer.Start();
									}
									currentState = newCurrentState;
									mustUpdateValues = true;
								}
								else
								{
									//Who knows what it is
									VerboseTrace("[EIL-R] Unexpected state input: " + newInput, 2);
								}
								//newInput = inputReader.ReadLine();
								//Trace.TraceInformation("[EIL-R] Read Made: " + newInput);
								if (!Int32.TryParse(newInputs[2], out minsToNextRecording))
								{
									VerboseTrace("[EIL-R] Unexpected input: " + newInput, 2);
								}
							}
							else if (newInputs[0].Equals("Status-State"))
							{
								//newInput = inputReader.ReadLine();
								//Trace.TraceInformation("[EIL-R] Read Made: " + newInput);
								State newCurrentState;
								if (Enum.TryParse(newInputs[1], true, out newCurrentState))
								{
									if (newCurrentState.Equals(currentState))
									{
										VerboseTrace("[EIL-R] Synchronisation with recorder confirmed");
									}
									else
									{
										VerboseTrace("[EIL-R] Recorder appears out of sync; updating now", 1);
										if (!isTransitioning)
										{
											isTransitioning = HasTransitioned(newCurrentState);
										}
										if (isTransitioning)
										{
											minsToNextRecording = 30000;
											//flashEndTimer.Start();
										}
										currentState = newCurrentState;
										mustUpdateValues = true;
									}
								}
								else
								{
									//Who knows what it is
									VerboseTrace("[EIL-R] Unexpected state input: " + newInput, 2);
								}
								//newInput = inputReader.ReadLine();
								//Trace.TraceInformation("[EIL-R] Read Made: " + newInput);
								if (!Int32.TryParse(newInputs[2], out minsToNextRecording))
								{
									VerboseTrace("[EIL-R] Unexpected input: " + newInput, 2);
								}
							}
							else
							{
								VerboseTrace("[EIL-R] Unhandled input: " + newInputs[0], 2);
							}

							if(mustUpdateValues)
                            {
								currentLightState = UpdateLightValues();
								mustUpdateValues = false;
							}

							VerboseTrace("[EIL-R] Checking for Reads");
						}
					}
					catch(Exception e)
                    {
						VerboseTrace("[EIL-R] Exception Thrown in Pipe Read - " + e.ToString());
                    }

					//Thread.Sleep(20);
				}
				pipeServerIn.Close();
				pipeServerOut.Close();
			}
		}

		private void NamedPipeWriterThread()
		{
			Trace.TraceInformation("[EIL-W] Writer Thread Active");
			while (true)
			{
				WaitHandle.WaitAny(new WaitHandle[] { this.inputQueueEvent });
				while (writerQueue.Count > 0)
				{
					string output = writerQueue.Dequeue();
					VerboseTrace("[EIL-W] Making Write - " + output);
					outputWriter.WriteLine(output);
					//Thread.Sleep(100);
					outputWriter.Flush();
					VerboseTrace("[EIL-W] Written");
				}
				this.inputQueueEvent.Reset();
				//Thread.Sleep(200);
			}
		}

		private void WriteToRecorder(string input)
        {
			if(isConnectedToRecorder)
            {
				writerQueue.Enqueue(input);
				this.inputQueueEvent.Set();
            }
        }

        #endregion

        #region Serial Input Handler

        /// <summary>
        /// Handle events sent by our portObj
        /// </summary>
        private void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
		{
			SerialPort sp = (SerialPort)sender;

			while (sp.IsOpen && sp.BytesToRead > 0)
			{
				string inputString = sp.ReadLine().TrimEnd('\r');
				//Command inputCommand;

				VerboseTrace("[" + sp.PortName.ToString() + "] Serial Input: " + inputString);

				//State currentState = this.stateMachine.GetCurrentState();

				switch (inputString[0])
				{
					case 'P':
						/// PRESS
						if (Properties.Settings.Default.EnableButton)
						{
							char inputCharTwo;
							if (inputString[1] == ' ')
							{
								inputCharTwo = inputString[2];
							}
							else
							{
								inputCharTwo = inputString[1];
							}
							switch (inputCharTwo)
							{
								case 'L':
									if (currentState.Equals(State.PreviewingWithNextSchedule) || currentState.Equals(State.StoppedWithNextSchedule))
									{
										VerboseTrace("[MAIN] Attempting to Opt Out...");
										WriteToRecorder("CommandOptOut");
									}
									else if (currentState.Equals(State.Recording) || currentState.Equals(State.RecordingWithinEndingWindow) || currentState.Equals(State.Paused))
									{
										WriteToRecorder("CommandStop");
									}
									break;
								case '1':
									if (currentState.Equals(State.Recording) || currentState.Equals(State.RecordingWithinEndingWindow))
									{
										WriteToRecorder("CommandPause");
									}
									else if (currentState.Equals(State.Paused))
									{
										WriteToRecorder("CommandResume");
									}
									break;
								case '2':
									if (currentState.Equals(State.PreviewingNoNextSchedule) || currentState.Equals(State.PreviewingWithNextSchedule) || currentState.Equals(State.StoppedNoNextSchedule) || currentState.Equals(State.StoppedWithNextSchedule))
									{
										if (Properties.Settings.Default.EnableAdHocStart)
										{
											WriteToRecorder("CommandStart");
										}
									}
									else if (currentState.Equals(State.Recording) || currentState.Equals(State.RecordingWithinEndingWindow) || currentState.Equals(State.Paused))
									{
										if (Properties.Settings.Default.EnableExtend)
										{
											WriteToRecorder("CommandExtend");
										}
									}
									break;
								case '3':
									/// TRIPLE- Currently unused
									break;
								case '6':
									isDanMash = true;
									danMashTimer.Start();
									currentLightState = UpdateLightValues();
									break;
								default:
									break;
							}
						}
						break;
					case 'S':
						/// Work out which light sent it
						Light speakingLight = new Light();
						int indexNum = 0;
						for (int i = 0; i < portObjList.Count; i++)// Light portObj in portObjList)
						{
							speakingLight = lightList[i];
							if (speakingLight.port.Equals(sp))
							{
								indexNum = i;
								break;
							}
						}
						if (speakingLight.Equals(null))
						{
							VerboseTrace("[MAIN] So apparently, none of the known lights sent this message. Huh.", 2);
							return;
						}
						speakingLight.hasResponded = true;

						/// HANDLING
						inputString = inputString.Replace(" ", "");
						inputString = inputString.Substring(1);
						string[] inputParameters = inputString.Split(',');

						if (Int32.TryParse(inputParameters[0], out int nRedPrimary) && Int32.TryParse(inputParameters[1], out int nGreenPrimary) && Int32.TryParse(inputParameters[2], out int nRedSecondary) && Int32.TryParse(inputParameters[3], out int nGreenSecondary) && Char.TryParse(inputParameters[4], out char nAnimation) && Int32.TryParse(inputParameters[5], out int nPeriod) && Int32.TryParse(inputParameters[6], out int nMisc))
						{
							if ((nRedPrimary == currentLightState.redPrimary) && (nGreenPrimary == currentLightState.greenPrimary) && (nRedSecondary == currentLightState.redSecondary) && (nGreenSecondary == currentLightState.greenSecondary) && (nAnimation.Equals(currentLightState.animation)) && (nPeriod == currentLightState.period) && (nMisc == currentLightState.miscData))
							{
								VerboseTrace("[MAIN] State Match");
								speakingLight.numberFails = 0;

							}
							else
							{
								VerboseTrace("[MAIN] State Mismatch", 2);
								speakingLight.numberFails += 1;
							}
						}
						else
						{
							VerboseTrace("[MAIN] Warning: " + sp.PortName.ToString() + " sent an invalid status code; light may be corrupted", 1);
							speakingLight.numberFails += 1;
						}

						lightList[indexNum] = speakingLight;
						break;
					case 'E':
						// ECHO CURRENTLY UNHANDLED
						break;
					case 'Z':
						// TODO: ERROR
						VerboseTrace("[MAIN] A previous command was not understood by the light", 2);
						break;
					default:
						VerboseTrace("[MAIN] Unhandled Input: " +  inputString, 2);
						break;
				}
			}
		}

		#endregion

		#region Output Handling

		private void Output(String str)
		{
			VerboseTrace("[MAIN] Serial Output: " + str);
			foreach (Light portObj in lightList)
			{
				try
				{
					portObj.port.WriteLine(str);
				}
				catch (Exception)
				{
					VerboseTrace("[MAIN] " + portObj.port.PortName.ToString() + " Connection Lost");
				}
			}
		}

		private void OutputConfigs(SerialPort portObj)
		{
			portObj.WriteLine("Cb" + Properties.Settings.Default.BufferTime.ToString());
			portObj.WriteLine("Ci" + Properties.Settings.Default.InterPressMaxTime.ToString());
			portObj.WriteLine("Cl" + Properties.Settings.Default.TimeLongPress.ToString());
			portObj.WriteLine("Cd" + Properties.Settings.Default.DebounceTime.ToString());
			portObj.WriteLine("Cc" + Properties.Settings.Default.AnimationRefreshRate.ToString());
		}

		private void OutputLight(LightOutput lightState)
		{
			HidOutput(lightState.hidOutputLED);
			this.Output("L " + lightState.redPrimary.ToString() + "," + lightState.greenPrimary.ToString() + "," + lightState.redSecondary.ToString() + "," + lightState.greenSecondary.ToString() + "," + lightState.animation.ToString() + "," + lightState.period.ToString() + "," + lightState.miscData.ToString());
			if (Properties.Settings.Default.EnableInterlock)
			{
				this.Output("S");
				//statusStackHeight += 1;
			}
		}

		#endregion

		#region COM Port Handling

		private void DeviceChangedEventHandler(object sender, EventArrivedEventArgs e)
		{
			if ((e.NewEvent.Properties["EventType"].Value.ToString().Equals("2")) || (e.NewEvent.Properties["EventType"].Value.ToString().Equals("3")))
			{
				List<string> newComList = new List<string>();
				List<string> oldComList = new List<string>();
				ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort");
				foreach (Light portObj in lightList)
				{
					oldComList.Add(portObj.port.PortName);
				}
				foreach (ManagementObject newPortObj in managementObjectSearcher.Get())
				{
					if (newPortObj["PNPDeviceID"].ToString().Contains("VID_04D8&PID_EE61"))
					{
						string newPortObjCom = newPortObj["DeviceID"].ToString();
						if (oldComList.Contains(newPortObjCom))
						{
							/// This port was already open
							oldComList.Remove(newPortObjCom);
						}
						else
						{
							/// This port is new to be opened
							try
							{
								SerialPort portObj = new SerialPort(
									newPortObj["DeviceID"].ToString(),
									Properties.Settings.Default.SerialPortBaudRate,
									(Parity)Enum.Parse(typeof(Parity), Properties.Settings.Default.SerialPortParity, true),
									Properties.Settings.Default.SerialPortDataBits,
									(StopBits)Enum.Parse(typeof(StopBits), Properties.Settings.Default.SerialPortStopBits, true));

								portObj.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
								portObj.DtrEnable = true;
								portObj.RtsEnable = true;

								portObj.Open();
								portObjList.Add(portObj);
								Light newLight = new Light();
								newLight.port = portObj;
								newLight.lightState = currentLightState;
								lightList.Add(newLight);
								numActiveLights += 1;
								//OutputConfigs(portObj);
								OutputLight(currentLightState);
								Trace.TraceInformation("[" + newPortObj["DeviceID"].ToString() + "] Device Connected through DeviceChangedEvent");
							}
							catch (Exception)
							{
								Trace.TraceInformation("[" + newPortObj["DeviceID"].ToString() + "] Device Failed to Connect through DeviceChangedEvent");
							}
						}
					}
				}
				if (oldComList.Count > 0)
				{
					/// An old one must have closed/ cannot be found
					foreach (string oldComPort in oldComList)
					{
						Trace.TraceInformation("[" + oldComPort + "] Device Disconnected through DeviceChangedEvent");
						foreach (Light portObj in lightList)
						{
							if (portObj.port.PortName.Equals(oldComPort))
							{
								portObj.port.Close();
								portObjList.Remove(portObj.port);
								lightList.Remove(portObj);
								numActiveLights -= 1;
								break;
							}
						}
					}
				}
			}
		}

		#endregion

		#region Getting Light Value from State
		private LightOutput UpdateLightValues()
		{
			VerboseTrace("[MAIN] Light Values being Updated");
			VerboseTrace("[MAIN] Time until next recording: " + minsToNextRecording);
			//TraceVerbose.Trace("L " + currentLightState.redPrimary.ToString() + "," + currentLightState.greenPrimary.ToString() + "," + currentLightState.redSecondary.ToString() + "," + currentLightState.greenSecondary.ToString() + "," + currentLightState.animation.ToString() + "," + currentLightState.period.ToString() + "," + currentLightState.miscData.ToString());
			LightOutput newLightState = new LightOutput();
			if (isDanMash)
			{
				newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
				newLightState.redSecondary = 0;
				newLightState.greenPrimary = 0;
				newLightState.greenSecondary = Properties.Settings.Default.GreenBrightness;
				newLightState.animation = 'B';
				newLightState.period = 1000;
				newLightState.miscData = 0;
				newLightState.hidOutputLED = danMash;
				danMashTimer.Start();
				//isRecording = false;
				//isPaused = false;
				//isTransitioning = false;
				//isExtend = false;
			}
			else if (isExtend)
			{
				if (isPaused)
				{
					newLightState.redPrimary = Properties.Settings.Default.AmberRedBrightness;
					newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
					newLightState.hidOutputLED = orangeChase;
				}
				else
				{
					newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
					newLightState.greenPrimary = 0;
					newLightState.hidOutputLED = redChase;
				}
				newLightState.redSecondary = 0;
				newLightState.greenSecondary = 0;
				newLightState.animation = 'R';
				newLightState.period = Properties.Settings.Default.ChasePeriod;
				newLightState.miscData = 2;
				chaseEndTimer.Start();
				//isRecording = false;
				//isPaused = false;
				//isTransitioning = false;
			}
			else if (isTransitioning)
			{
				switch (transitionType)  /// 1 = Stopped to Rec, 2 = Rec to Stopped, 3 = Rec to Paused, 4 = Paused to Rec, 5 = Paused to Stopped, 6 = Stopped to Paused
				{
					case 1: /// Stopped to Recording
						newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
						newLightState.redSecondary = 0;
						newLightState.greenPrimary = 0;
						newLightState.greenSecondary = Properties.Settings.Default.GreenBrightness;
						newLightState.hidOutputLED = greenRedFlash;
						break;
					case 2: /// Recording to Stopped
						newLightState.redPrimary = 0;
						newLightState.redSecondary = Properties.Settings.Default.RedBrightness;
						newLightState.greenPrimary = Properties.Settings.Default.GreenBrightness;
						newLightState.greenSecondary = 0;
						newLightState.hidOutputLED = greenRedFlash;
						break;
					case 3: /// Recording to Paused
						newLightState.redPrimary = Properties.Settings.Default.AmberRedBrightness;
						newLightState.redSecondary = Properties.Settings.Default.RedBrightness;
						newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
						newLightState.greenSecondary = 0;
						newLightState.hidOutputLED = redOrangeFlash;
						break;
					case 4: /// Paused to Recording
						newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
						newLightState.redSecondary = Properties.Settings.Default.AmberRedBrightness;
						newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
						newLightState.greenSecondary = 0;
						newLightState.hidOutputLED = redOrangeFlash;
						break;
					case 5: /// Paused to Stopped
						newLightState.redPrimary = 0;
						newLightState.redSecondary = Properties.Settings.Default.AmberRedBrightness;
						newLightState.greenPrimary = Properties.Settings.Default.GreenBrightness;
						newLightState.greenSecondary = Properties.Settings.Default.AmberGreenBrightness;
						newLightState.hidOutputLED = orangeGreenFlash;
						break;
					case 6: /// Stopped to Paused
						newLightState.redPrimary = Properties.Settings.Default.AmberRedBrightness;
						newLightState.redSecondary = 0;
						newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
						newLightState.greenSecondary = Properties.Settings.Default.GreenBrightness;
						newLightState.hidOutputLED = orangeGreenFlash;
						break;
					default:
						//Invalid Transition Type
						break;
				}
				newLightState.animation = 'F';
				newLightState.period = Properties.Settings.Default.FlashPeriod;
				newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
				flashEndTimer.Start();
			}
			else if (currentState.Equals(State.Init))
			{
				newLightState.redPrimary = 0;
				newLightState.redSecondary = 0;
				newLightState.greenPrimary = 0;
				newLightState.greenSecondary = 0;
				newLightState.animation = 'O';
				newLightState.period = 0;
				newLightState.miscData = 0;
				newLightState.hidOutputLED = lightInit;
				isRecording = false;
				isPaused = false;
			}
			else if (currentState.Equals(State.PreviewingNoNextSchedule) || currentState.Equals(State.StoppedNoNextSchedule) || currentState.Equals(State.TransitionPausedToStop) || currentState.Equals(State.TransitionRecordingToStop))
			{
				newLightState.redPrimary = 0;
				newLightState.redSecondary = 0;
				newLightState.greenPrimary = Properties.Settings.Default.PreviewingBrightness * Properties.Settings.Default.GreenBrightness / 255;
				newLightState.greenSecondary = 0;
				newLightState.animation = 'O';
				newLightState.period = 0;
				newLightState.miscData = 0;
				newLightState.hidOutputLED = greenSolidDim;
				isRecording = false;
				isPaused = false;
			}
			else if (currentState.Equals(State.StoppedWithNextSchedule) || currentState.Equals(State.PreviewingWithNextSchedule) || currentState.Equals(State.PotentialRecording))
			{
				newLightState.redPrimary = 0;
				newLightState.redSecondary = 0;
				newLightState.greenSecondary = 0;
				newLightState.greenPrimary = Properties.Settings.Default.GreenBrightness;
				if (minsToNextRecording > Properties.Settings.Default.BreatheMins)
				{
					newLightState.animation = 'O';
					newLightState.period = 0;
					newLightState.miscData = 0;
					newLightState.hidOutputLED = greenSolidBright;
				}
				else
				{
					newLightState.animation = 'B';
					newLightState.period = Properties.Settings.Default.BreathePeriod;
					newLightState.miscData = 0;
					newLightState.hidOutputLED = greenBreathe;
				}
				isRecording = false;
				isPaused = false;
			}
			//else if (currentState.Equals(State.TransitionAnyToRecording))
			//{
			//	newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
			//	newLightState.redSecondary = 0;
			//	newLightState.greenPrimary = 0;
			//	newLightState.greenSecondary = Properties.Settings.Default.GreenBrightness;
			//	newLightState.animation = 'F';
			//	newLightState.period = Properties.Settings.Default.FlashPeriod;
			//	newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
			//	newLightState.hidOutputLED = greenRedFlash;
			//	flashEndTimer.Start();
			//	isRecording = true;
			//	isPaused = false;
			//}
			else if (currentState.Equals(State.Recording) || currentState.Equals(State.TransitionAnyToRecording) || currentState.Equals(State.RecordingWithinEndingWindow) || currentState.Equals(State.TransitionPausedToRecording) || currentState.Equals(State.PotentialRecording))
			{
				newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
				newLightState.redSecondary = 0;
				newLightState.greenPrimary = 0;
				newLightState.greenSecondary = 0;
				if (minsToNextRecording < Properties.Settings.Default.BreatheMins)
				{
					newLightState.animation = 'B';
					newLightState.period = Properties.Settings.Default.BreathePeriod;
					newLightState.hidOutputLED = redBreathe;
				}
				else
				{
					newLightState.animation = 'O';
					newLightState.period = 0;
					newLightState.hidOutputLED = redSolidBright;
				}
				newLightState.miscData = 0;
				isRecording = true;
				isPaused = false;
			}
			//else if (currentState.Equals(State.RecordingWithinEndingWindow))
			//{
			//	newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
			//	newLightState.redSecondary = 0;
			//	newLightState.greenPrimary = 0;
			//	newLightState.greenSecondary = 0;
			//	newLightState.animation = 'B';
			//	newLightState.period = Properties.Settings.Default.BreathePeriod;
			//	newLightState.miscData = 0;
			//	newLightState.hidOutputLED = redBreathe;
			//	isRecording = true;
			//	isPaused = false;
			//}
			//else if (currentState.Equals(State.TransitionRecordingToPause))
			//{
			//	newLightState.redPrimary = Properties.Settings.Default.AmberRedBrightness;
			//	newLightState.redSecondary = Properties.Settings.Default.RedBrightness;
			//	newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
			//	newLightState.greenSecondary = 0;
			//	newLightState.animation = 'F';
			//	newLightState.period = Properties.Settings.Default.FlashPeriod;
			//	newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
			//	newLightState.hidOutputLED = redOrangeFlash;
			//	flashEndTimer.Start();
			//	isRecording = true;
			//	isPaused = true;
			//}
			//else if (currentState.Equals(State.TransitionPausedToRecording))
			//{
			//	newLightState.redPrimary = Properties.Settings.Default.RedBrightness;
			//	newLightState.redSecondary = Properties.Settings.Default.AmberRedBrightness;
			//	newLightState.greenPrimary = 0;
			//	newLightState.greenSecondary = Properties.Settings.Default.AmberGreenBrightness;
			//	newLightState.animation = 'F';
			//	newLightState.period = Properties.Settings.Default.FlashPeriod;
			//	newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
			//	newLightState.hidOutputLED = redOrangeFlash;
			//	flashEndTimer.Start();
			//	isRecording = true;
			//	isPaused = false;
			//}
			else if (currentState.Equals(State.Paused) || currentState.Equals(State.TransitionRecordingToPause))
			{
				newLightState.redPrimary = Properties.Settings.Default.AmberRedBrightness;
				newLightState.redSecondary = 0;
				newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
				newLightState.greenSecondary = 0;
				if (minsToNextRecording < Properties.Settings.Default.BreatheMins)
				{
					newLightState.animation = 'B';
					newLightState.period = Properties.Settings.Default.BreathePeriod;
					newLightState.hidOutputLED = orangeBreathe;
				}
				else
				{
					newLightState.animation = 'O';
					newLightState.period = 0;
					newLightState.hidOutputLED = orangeSolidBright;
				}
				newLightState.miscData = 0;
				isRecording = true;
				isPaused = true;
			}
			//else if (currentState.Equals(State.TransitionPausedToStop))
			//{
			//	newLightState.redPrimary = 0;
			//	newLightState.redSecondary = Properties.Settings.Default.AmberRedBrightness;
			//	newLightState.greenPrimary = Properties.Settings.Default.GreenBrightness;
			//	newLightState.greenSecondary = Properties.Settings.Default.AmberGreenBrightness;
			//	newLightState.animation = 'F';
			//	newLightState.period = Properties.Settings.Default.FlashPeriod;
			//	newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
			//	newLightState.hidOutputLED = orangeGreenFlash;
			//	flashEndTimer.Start();
			//	isRecording = false;
			//	isPaused = false;
			//}
			//else if (currentState.Equals(State.TransitionRecordingToStop))
			//{
			//	newLightState.redPrimary = 0;
			//	newLightState.redSecondary = Properties.Settings.Default.RedBrightness;
			//	newLightState.greenPrimary = Properties.Settings.Default.GreenBrightness;
			//	newLightState.greenSecondary = 0;
			//	newLightState.animation = 'F';
			//	newLightState.period = Properties.Settings.Default.FlashPeriod;
			//	newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
			//	newLightState.hidOutputLED = greenRedFlash;
			//	flashEndTimer.Start();
			//	isRecording = false;
			//	isPaused = false;
			//}
			else
			{
				newLightState.redPrimary = Properties.Settings.Default.AmberRedBrightness;
				newLightState.redSecondary = 0;
				newLightState.greenPrimary = Properties.Settings.Default.AmberGreenBrightness;
				newLightState.greenSecondary = 0;
				newLightState.animation = 'F';
				newLightState.period = Properties.Settings.Default.FlashPeriod;
				newLightState.miscData = Properties.Settings.Default.FlashPeriod / 2;
				newLightState.hidOutputLED = lightError;
				isRecording = false;
				isPaused = false;
			}
			//TraceVerbose.Trace("L " + newLightState.redPrimary.ToString() + "," + newLightState.greenPrimary.ToString() + "," + newLightState.redSecondary.ToString() + "," + newLightState.greenSecondary.ToString() + "," + newLightState.animation.ToString() + "," + newLightState.period.ToString() + "," + newLightState.miscData.ToString());

			OutputLight(newLightState);
			return newLightState;
		}

		#endregion

		#region Checking for Transition

		private bool HasTransitioned(State newCurrentState)
		{
			if (newCurrentState.Equals(State.Paused) || newCurrentState.Equals(State.TransitionRecordingToPause))
			{
				///Is Paused
				if (isPaused)
				{
					if (isTransitioning)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					if (isRecording)
					{
						transitionType = 3;
					}
					else
					{
						transitionType = 6;
					}
					isPaused = true;
					isRecording = true;
					return true;
				}
			}
			else if (newCurrentState.Equals(State.PotentialRecording) || newCurrentState.Equals(State.Recording) || newCurrentState.Equals(State.RecordingWithinEndingWindow) || newCurrentState.Equals(State.TransitionAnyToRecording) || newCurrentState.Equals(State.TransitionPausedToRecording))
			{
				/// Is Recording, but Not Paused
				if (isRecording && !isPaused)
				{
					if (isTransitioning)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					if (isPaused)
					{
						transitionType = 4;
					}
					else
					{
						transitionType = 1;
					}
					isPaused = false;
					isRecording = true;
					return true;
				}
			}
			else
			{
				if (!isRecording)
				{
					if (isTransitioning)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				else
				{
					if (isPaused)
					{
						transitionType = 5;
					}
					else
					{
						transitionType = 2;
					}
					isPaused = false;
					isRecording = false;
					return true;
				}
			}

		}

		#endregion

		#region Timers

		private void StatePingTimerEvent(Object source, ElapsedEventArgs e)
		{
			VerboseTrace("[MAIN] New State Ping");
			WriteToRecorder("Status");
			//HidOutput(); TODO INTERLOCK LIGHTS
			
			/// Check there is a valid light attached
			if (hidLightStructList.Count.Equals(0) && isRecording && !isPaused)
			{
				Trace.TraceWarning("[MAIN] No Lights Connected; pausing recording");
				WriteToRecorder("CommandPause");
			}

			if(!isConnectedToRecorder)
            {
				Trace.TraceWarning("[MAIN] No Software Connected; showing error state on lights");
				currentState = State.Faulted;
				currentLightState = UpdateLightValues();
			}
			else
            {
				double timeSinceLastPipeRead = (DateTime.Now.ToLocalTime() - lastPipeReadTime).TotalSeconds;
				VerboseTrace("[MAIN] Time Since Last Pipe Read: " + timeSinceLastPipeRead.ToString());
				if (timeSinceLastPipeRead > 60)
				{
					Trace.TraceWarning("[MAIN] Software not responded in 60s; showing error state on lights");
					currentState = State.Faulted;
					currentLightState = UpdateLightValues();
				}
			}
			
			//int numberAccurateLights = portObjList.Count;
			//for (int i = 0; i < portObjList.Count; i++)
			//{
			//	Light currentEIL = lightList[i];
			//	if (!currentEIL.hasResponded)
			//	{
			//		currentEIL.numberFails += 1;
			//	}
			//	Trace.TraceInformation(currentEIL.numberFails.ToString());
			//	if (currentEIL.numberFails >= 3)
			//	{
			//		Trace.TraceError("[MAIN] Error: " + currentEIL.port.PortName.ToString() + " Light Failure");
			//		numberAccurateLights -= 1;
			//	}
			//	currentEIL.hasResponded = false;
			//	lightList[i] = currentEIL;
			//}
			//if (isRecording && !isPaused && numberAccurateLights == 0)
			//{
			//	WriteToRecorder("CommandPause");
			//	this.inputQueueEvent.Set();
			//}
			///// Ping EILs
			//this.Output("S");
		}

        private void heldTimerElapsedEvent(Object source, ElapsedEventArgs e, System.Timers.Timer heldTimer, HidDevice lightDev)
		{
			VerboseTrace("[HLDTIM] Held Timer Elapsed");
			HIDLight lightStruct = hidLightStructList[0];
			
			int lightStructNum = 0;
			for(lightStructNum = 0; lightStructNum < hidLightStructList.Count; lightStructNum ++)
            {
				HIDLight lightStructTemp = hidLightStructList[lightStructNum];
				if(lightStructTemp.device.Equals(lightDev))
                {
					lightStruct = lightStructTemp;
					break;
                }
            }

			heldTimer.Stop();
			if (lightStruct.isHeld)
			{
				lightStruct.isLongPress = true;
				if(Properties.Settings.Default.EnableHapticOnLongPress)
                {
					HidOutput(motorDoubleBuzz);
				}
				if (Properties.Settings.Default.EnableBuzzerOnLongPress)
				{
					HidOutput(soundDoubleBeep);
				}


				if (currentState.Equals(State.PreviewingWithNextSchedule) || currentState.Equals(State.PreviewingNoNextSchedule) || currentState.Equals(State.StoppedWithNextSchedule) || currentState.Equals(State.StoppedNoNextSchedule))
				{
					if (Properties.Settings.Default.EnableButton)
                    {
						WriteToRecorder("CommandOptOut");
					}
				}
				else if(currentState.Equals(State.Recording) || currentState.Equals(State.RecordingWithinEndingWindow) || currentState.Equals(State.Paused))
                {
					if (Properties.Settings.Default.EnableButton)
					{
						WriteToRecorder("CommandStop");
					}
				}
			}
			lightStruct.numberOfPresses = 0;
			hidLightStructList[lightStructNum] = lightStruct;
		}

		private void releaseTimerElapsedEvent(Object source, ElapsedEventArgs e, System.Timers.Timer releaseTimer, HidDevice lightDev)
		{
			VerboseTrace("[RLSTIM] Release Timer Elapsed");
			HIDLight lightStruct = hidLightStructList[0];

			int lightStructNum;
			for (lightStructNum = 0; lightStructNum < hidLightStructList.Count; lightStructNum++)
			{
				HIDLight lightStructTemp = hidLightStructList[lightStructNum];
				if (lightStructTemp.device.Equals(lightDev))
				{
					lightStruct = lightStructTemp;
					break;
				}
			}

			releaseTimer.Stop();
			chaseEndTimer.Stop();
			flashEndTimer.Stop();
			danMashTimer.Stop();
			switch (lightStruct.numberOfPresses)
			{
				case 1: // Single Press
					if (currentState.Equals(State.Paused) || currentState.Equals(State.TransitionRecordingToPause))
					{
						if (Properties.Settings.Default.EnableButton)
						{
							WriteToRecorder("CommandResume");
						}
					}
					else if (currentState.Equals(State.Recording) || currentState.Equals(State.TransitionPausedToRecording) || currentState.Equals(State.TransitionAnyToRecording))
					{
						if (Properties.Settings.Default.EnableButton)
						{ 
							WriteToRecorder("CommandPause");
						}
					}
					break;
				case 2: // Double Press
					if (currentState.Equals(State.PreviewingWithNextSchedule) || currentState.Equals(State.PreviewingNoNextSchedule) || currentState.Equals(State.StoppedWithNextSchedule) || currentState.Equals(State.StoppedNoNextSchedule))
					{
						if(Properties.Settings.Default.EnableAdHocStart && Properties.Settings.Default.EnableButton)
                        {
							WriteToRecorder("CommandStart");
						}
					}
					else if (currentState.Equals(State.Recording) || currentState.Equals(State.Paused) || currentState.Equals(State.RecordingWithinEndingWindow))
					{
						if(Properties.Settings.Default.EnableExtend && Properties.Settings.Default.EnableButton)
                        {
							WriteToRecorder("CommandExtend");
						}
					}
					break;
				case 6: // DanMash
					isDanMash = true;
					HidOutput(danMash);
					danMashTimer.Start();
					//this.Output("");
					break;
				case 0:
					if (!lightStruct.isLongPress)
					{
						VerboseTrace("Unhandled Number of Presses");
					}
					lightStruct.isLongPress = false;
					break;
				default:
					VerboseTrace("Unhandled Number of Presses");
					break;
			}
			lightStruct.numberOfPresses = 0;
			hidLightStructList[lightStructNum] = lightStruct;
		}

		private void FlashEndTimerEvent(object source, ElapsedEventArgs e)
		{
			isTransitioning = false;
			currentLightState = UpdateLightValues();
			flashEndTimer.Stop();
		}

		private void ChaseEndTimerEvent(object source, ElapsedEventArgs e)
		{
			isExtend = false;
			currentLightState = UpdateLightValues();
			chaseEndTimer.Stop();
		}

		private void DanMashTimerEvent(object source, ElapsedEventArgs e)
		{
			isDanMash = false;
			currentLightState = UpdateLightValues();
			danMashTimer.Stop();
		}

        #endregion

        #region Trace Handling
        private void VerboseTrace(string i, int errorLevel)
		{
			if(useVerboseTrace)
            {
				if (errorLevel == 2)
				{
					Trace.TraceError(i);
				}
				else if (errorLevel == 1)
                {
					Trace.TraceWarning(i);
				}
				else
                {
					Trace.TraceInformation(i);
				}
            }
		}

		private void VerboseTrace(string i)
		{
			if (useVerboseTrace)
			{
				Trace.TraceInformation(i);
			}
		}

		#endregion
	}
}



