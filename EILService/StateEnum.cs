﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EILService
{
	public enum State
	{
		Init,
		PreviewingNoNextSchedule,
		PreviewingWithNextSchedule,
		TransitionAnyToRecording,
		PotentialRecording,
		Recording,
		RecordingWithinEndingWindow,
		TransitionRecordingToPause,
		TransitionPausedToRecording,
		Paused,
		TransitionPausedToStop,
		TransitionRecordingToStop,
		StoppedNoNextSchedule,
		StoppedWithNextSchedule,
		Dormant,
		Faulted,
		Disconnected,
	}
}
