﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace EILService
{
	internal static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		private static void Main(string[] args)
		{
			var service = new EncoreService();
			if(Environment.UserInteractive)
            {
				// Run as Program
				// Set up Trace Log
				DirectoryInfo currentDir = new DirectoryInfo(Directory.GetCurrentDirectory());
				TextWriterTraceListener traceListen = new TextWriterTraceListener(Path.Combine(currentDir.FullName, "EIL_SERVICE_MAIN_LOG_OUT.log"));
				Trace.Listeners.Add(traceListen);
				Trace.AutoFlush = true;

				// Service Start
				EncoreService mainService = new EncoreService();
				mainService.RunAsProgram();
			}
			else
            {
				var listener = new EventLogTraceListener("EILService");
				Trace.Listeners.Add(listener);

				DirectoryInfo currentDir = new DirectoryInfo(Directory.GetCurrentDirectory());
				TextWriterTraceListener traceListen = new TextWriterTraceListener(Path.Combine(currentDir.FullName, "EIL_SERVICE_MAIN_LOG_OUT.log"));
				Trace.Listeners.Add(traceListen);
				Trace.AutoFlush = true;

				ServiceBase[] ServicesToRun;
				ServicesToRun = new ServiceBase[]
				{
					service
				};
				ServiceBase.Run(ServicesToRun);
			}

			

			
		}
	}
}
